﻿#undef BLATHER
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace JayTranslationTool.Core
{
	public abstract class Bindable : INotifyPropertyChanged
	{
		protected virtual void SetProperty<T>(ref T member, T val, [CallerMemberName] string propertyName = null)
		{
			if (object.Equals(member, val))
			{
				return;
			}

			member = val;
			RaisePropertyChanged(propertyName);
		}

		protected virtual void RaisePropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}
