﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using System.Runtime.CompilerServices;
//using System.Windows.Controls;

//namespace JayTranslationTool
//{
//	public class ValidatableBindable : Bindable, INotifyDataErrorInfo
//	{
//		private Dictionary<string, List<string>> _errors = new Dictionary<string, List<string>>();

//		public bool HasErrors => _errors.Any();

//		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

//		public IEnumerable GetErrors(string propertyName)
//		{
//			if (string.IsNullOrEmpty(propertyName) && _errors.Count > 0) {
//				return _errors.ElementAt(0).Value;
//			} else if (_errors.ContainsKey(propertyName)) {
//				return _errors[propertyName];
//			} else {
//				return null;
//			}
//		}

//		public virtual void RaiseErrorsChanged(string propertyName)
//		{
//			ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
//		}

//		protected override void SetProperty<T>(ref T member, T val, [CallerMemberName] string propertyName = null)
//		{
//			base.SetProperty(ref member, val, propertyName);
//			ValidateProperty(propertyName, val);
//		}

//		private void ValidateProperty<T>(string propertyName, T val)
//		{
//			List<ValidationResult> results = new List<ValidationResult>();
//			ValidationContext context = new ValidationContext(this);
//			context.MemberName = propertyName;
//			Validator.TryValidateProperty(val, context, results);

//			if (results.Any()) {
//				_errors[propertyName] = results.Select(c => c.ErrorMessage).ToList();
//			} else {
//				_errors.Remove(propertyName);
//			}

//			RaiseErrorsChanged(propertyName);
//		}

//		public void InvalidateProperty(string propertyName, string errorMessage)
//		{
//			_errors[propertyName] = new List<string>() { errorMessage };

//			RaiseErrorsChanged(propertyName);
//		}

//		public void ClearValidationErrors(string propertyName)
//		{
//			if (_errors.ContainsKey(propertyName))
//				_errors.Remove(propertyName);

//			RaiseErrorsChanged(propertyName);
//		}
//	}
//}
