﻿using JayTranslationTool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JayTranslationTool.Modules.Main.Models
{
    public class TranslationOutput : Bindable
    {
        private string _label;
        private string _translation;

        public string Label
        {
            get
            {
                return _label;
            }

            set
            {
                _label = value;
                RaisePropertyChanged(nameof(Label));
            }
        }

        public string Translation
        {
            get
            {
                return _translation;
            }

            set
            {
                _translation = value;
                RaisePropertyChanged(nameof(Translation));
            }
        }
    }
}
