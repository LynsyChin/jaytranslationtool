﻿using JayTranslationTool.Core;
using JayTranslationTool.Modules.Main.Collections;
using JayTranslationTool.Modules.Main.Models;
using JayTranslationTool.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JayTranslationTool.Modules.Main.ViewModels
{
    public class MainWindowViewModel : Bindable
    {
        #region Fields

        private string _userInput;
        private string _userInputLabel;
        private string _outputLabel;
        private string _userInputPlaceholder;

        private TranslationOutputs _translations;
        private JType _detectedUserInputLanguage;

        //Hiragana to Katakana
        Dictionary<char, char> hi_ka = new Dictionary<char, char>();
        //Katakana to Hiragana
        Dictionary<char, char> ka_hi = new Dictionary<char, char>();

        //Hiragana to Romaji
        Dictionary<char, String> hi_ro = new Dictionary<char, String>();
        //Katakana to Romaji
        Dictionary<char, String> ka_ro = new Dictionary<char, String>();

        //Romaji to Katakana
        Dictionary<char, char> ro_ka = new Dictionary<char, char>();
        //Romaji to Hiragana
        Dictionary<char, char> ro_hi = new Dictionary<char, char>();

        #endregion

        #region Constructor

        public MainWindowViewModel() {
            InitializeFields();
            LoadAvailableTranslations();

            AddTranslation = new RelayCommand(OnAddTranslation);
            ResetInput = new RelayCommand(OnResetInput);
            Translate = new RelayCommand(OnTranslate);
        }

        #endregion

        #region ENUMs

        enum Mode
        {
            Unknown,
            Hiragana_Katakana,
            Katakana_Hiragana,
            Katakana_Romaji,
            Hiragana_Romaji,
            Romaji_Katakana,
            Romaji_Hiragana
        }

        public enum JType
        {
            Unknown,
            Hiragana,
            Katakana,
            Romaji
        }

        #endregion

        #region Methods

        private void InitializeFields()
        {
            UserInput = string.Empty;
            UserInputLabel = Resources.Input_Label;
            UserInputPlaceholder = Resources.Input_Placeholder;
            OutputLabel = Resources.Output_Label;

            Translations = new TranslationOutputs();
            Translations.Add(new TranslationOutput() { Label = Resources.TranslationLabel_Katakana});
            Translations.Add(new TranslationOutput() { Label = Resources.TranslationLabel_Hiragana});
            Translations.Add(new TranslationOutput() { Label = Resources.TranslationLabel_Romaji});
        }

        private void OnAddTranslation(object obj)
        {
            TranslationOutput to = obj as TranslationOutput;

            if(obj != null)
            {
                Translations.Add(to);
            }
        }

        private void OnResetInput(object obj)
        {
            UserInput = string.Empty;
        }

        private void OnTranslate(object obj)
        {
            if (string.IsNullOrEmpty(UserInput))
            {
                foreach(TranslationOutput output in Translations)
                {
                    output.Translation = string.Empty;
                }
                return;
            }

            DetectedUserInputLanguage = DetectJLanguage(UserInput);

            if (DetectedUserInputLanguage == JType.Hiragana)
            {
                TranslationOutput to = Translations.FirstOrDefault(x => x.Label == JType.Hiragana.ToString());
                if (to != null)
                {
                    to.Translation = UserInput;
                }

                foreach (TranslationOutput output in Translations)
                {
                    if (output.Label != JType.Hiragana.ToString())
                    {
                        Mode translationMode = FindTranslationMode(DetectedUserInputLanguage, output.Label);
                        output.Translation = Replace(UserInput, translationMode);
                    }
                }
            }
        }

        private string Replace(string input, Mode convertMode)
        {
            switch(convertMode)
            {
                case Mode.Hiragana_Katakana : return HiraToKata(input);
                case Mode.Katakana_Hiragana: return KataToHira(input);
                case Mode.Hiragana_Romaji: return HiraToRoma(input);
                case Mode.Katakana_Romaji: return KataToRoma(input);
            }
            return "";
        }

        private string HiraToKata(string input)
        {
            string trans = "";
            for(int i = 0; i < input.Length; i++)
            {
                // To Do: add # condition
                char value;
                if (hi_ka.TryGetValue(input.ElementAt(i), out value))
                {
                    trans += value; 
                }
                else
                {
                    if (input.ElementAt(i).CompareTo(' ') == 0)
                        trans += " ";
                    else
                        trans += "#";
                }
            }

            return trans;
        }

        private string KataToHira(string input)
        {
            string trans = "";
            for (int i = 0; i < input.Length; i++)
            {
                // To Do: add # condition
                char value;
                if (ka_hi.TryGetValue(input.ElementAt(i), out value))
                {
                    trans += value;
                }
                else
                {
                    if (input.ElementAt(i).CompareTo(' ') == 0)
                        trans += " ";
                    else
                        trans += "#";
                }
            }

            return trans;
        }

        private string HiraToRoma(string input)
        {
            string trans = "";
            for (int i = 0; i < input.Length; i++)
            {
                string value;
                if (hi_ro.TryGetValue(input.ElementAt(i), out value))
                {
                    if (i < input.Length-1 && !string.IsNullOrEmpty(NextCharacterCanTransformHiraToRoma(input.ElementAt(i), input.ElementAt(i+1))))
                    {
                        trans += NextCharacterCanTransformHiraToRoma(input.ElementAt(i), input.ElementAt(i + 1));
                        i++;
                    } else
                    {
                        trans += value;
                    }
                    
                }
                else
                {
                    if (input.ElementAt(i).CompareTo(' ') == 0)
                        trans += " ";
                    else
                        trans += "#";
                }
            }
            return trans;
        }

        private string KataToRoma(string input)
        {
            string trans = "";
            for (int i = 0; i < input.Length; i++)
            {
                string value;
                if (ka_ro.TryGetValue(input.ElementAt(i), out value))
                {
                    if (i < input.Length-1 && !string.IsNullOrEmpty(NextCharacterCanTransformKataToRoma(input.ElementAt(i), input.ElementAt(i + 1))))
                    {
                        trans += NextCharacterCanTransformKataToRoma(input.ElementAt(i), input.ElementAt(i + 1));
                        i++;
                    }
                    else
                    {
                        trans += value;
                    }
                }
                else
                {
                    if (input.ElementAt(i).CompareTo(' ') == 0)
                        trans += " ";
                    else
                        trans += "#";
                }
            }
            return trans;
        }

        private string NextCharacterCanTransformHiraToRoma(char c1, char c2)
        {
            string res = "" + c1 + c2;
            switch (res)
            {
                case "しゃ": return "sha";
                case "しゅ": return "shu";
                case "しょ": return "sho";
                case "じゃ": return "ja";
                case "じゅ": return "ju";
                case "じょ": return "jo";
                case "ちゃ": return "cha";
                case "ちゅ": return "chu";
                case "ちょ": return "cho";
                case "ふぁ": return "fa";
                case "ふぃ": return "fi";
                case "ふぇ": return "fe";
                case "ふぉ": return "fo";
                case "ゔぁ": return "va";
                case "ゔぇ": return "ve";
                case "ゔぃ": return "vi";
                case "ゔぉ": return "vo";
                case "きゃ": return "kya";
                case "きゅ": return "kyu";
                case "きょ": return "kyo";
                case "ぎゃ": return "gya";
                case "ぎゅ": return "gyu";
                case "ぎょ": return "gyo";
                case "にゃ": return "nya";
                case "にゅ": return "nyu";
                case "にょ": return "nyo";
                case "みゃ": return "mya";
                case "みゅ": return "my";
                case "みょ": return "myo";
                case "りゃ": return "rya";
                case "りゅ": return "ryu";
                case "りょ": return "ryo";
                case "ひゃ": return "hya";
                case "ひゅ": return "hyu";
                case "ひょ": return "hyo";
                case "びゃ": return "bya";
                case "びゅ": return "byu";
                case "びょ": return "byo";
                case "ぴゃ": return "pya";
                case "ぴゅ": return "pyu";
                case "ぴょ": return "pyo";
                case "んや": return "n'ya";
                case "んゆ": return "n'yu";
                case "んよ": return "n'yo";
                case "んあ": return "n'a";
                case "んい": return "n'i";
                case "んう": return "n'u";
                case "んえ": return "n'e";
                case "んお": return "n'o";
            }
            return "";
        }

        private string NextCharacterCanTransformKataToRoma(char c1, char c2)
        {
            string res = "" + c1 + c2;
            switch (res)
            {
                case "シャ": return "sha";
                case "シュ": return "shu";
                case "ショ": return "sho";
                case "ジャ": return "ja";
                case "ジュ": return "ju";
                case "ジョ": return "jo";
                case "チャ": return "cha";
                case "チュ": return "chu";
                case "チョ": return "cho";
                case "ファ": return "fa";
                case "フィ": return "fi";
                case "フェ": return "fe";
                case "フォ": return "fo";
                case "ヴァ": return "va";
                case "ヴェ": return "ve";
                case "ヴィ": return "vi";
                case "ヴォ": return "vo";
                case "キャ": return "kya";
                case "キュ": return "kyu";
                case "キョ": return "kyo";
                case "ギャ": return "gya";
                case "ギュ": return "gyu";
                case "ギョ": return "gyo";
                case "ニャ": return "nya";
                case "ニュ": return "nyu";
                case "ニョ": return "nyo";
                case "ミャ": return "mya";
                case "ミュ": return "my";
                case "ミョ": return "myo";
                case "リャ": return "rya";
                case "リュ": return "ryu";
                case "リョ": return "ryo";
                case "ヒャ": return "hya";
                case "ヒュ": return "hyu";
                case "ヒョ": return "hyo";
                case "ビャ": return "bya";
                case "ビュ": return "byu";
                case "ビョ": return "byo";
                case "ピャ": return "pya";
                case "ピュ": return "pyu";
                case "ピョ": return "pyo";
                case "ンヤ": return "n'ya";
                case "ンユ": return "n'yu";
                case "ンヨ": return "n'yo";
                case "ンア": return "n'a";
                case "ンイ": return "n'i";
                case "ンウ": return "n'u";
                case "ンエ": return "n'e";
                case "ンオ": return "n'o";
            }
            return "";
        }

        //Verifies input range inside the ASCII Table
        private static IEnumerable<char> GetCharsInRange(string text, int min, int max)
        {
            return text.Where(e => e >= min && e <= max);
        }

        //Detects if char is hiragana, katakana or romaji
        private static JType DetectJLanguage(string character)
        {
            var romaji = GetCharsInRange(character, 0x0020, 0x007E);
            var hiragana = GetCharsInRange(character, 0x3040, 0x309F);
            var katakana = GetCharsInRange(character, 0x30A0, 0x30FF);

            if (hiragana.Any())
            {
                return JType.Hiragana;
            } else if (katakana.Any())
            {
                return JType.Katakana;
            } else if(romaji.Any())
            {
                return JType.Romaji;
            }
            else
            {
                return JType.Unknown;
            }
        }

        private void LoadAvailableTranslations()
        {

            #region HirganaToKatakana
            hi_ka.Add('ゔ', 'ヴ');
            hi_ka.Add('か', 'カ');
            hi_ka.Add('き', 'キ');
            hi_ka.Add('く', 'ク');
            hi_ka.Add('け', 'ケ');
            hi_ka.Add('こ', 'コ');
            hi_ka.Add('が', 'ガ');
            hi_ka.Add('ぎ', 'ギ');
            hi_ka.Add('ぐ', 'グ');
            hi_ka.Add('げ', 'ゲ');
            hi_ka.Add('ご', 'ゴ');
            hi_ka.Add('さ', 'サ');
            hi_ka.Add('し', 'シ');
            hi_ka.Add('す', 'ス');
            hi_ka.Add('せ', 'セ');
            hi_ka.Add('そ', 'ソ');
            hi_ka.Add('ざ', 'ザ');
            hi_ka.Add('じ', 'ジ');
            hi_ka.Add('ず', 'ズ');
            hi_ka.Add('ぜ', 'ゼ');
            hi_ka.Add('ぞ', 'ゾ');
            hi_ka.Add('た', 'タ');
            hi_ka.Add('ち', 'チ');
            hi_ka.Add('つ', 'ツ');
            hi_ka.Add('て', 'テ');
            hi_ka.Add('と', 'ト');
            hi_ka.Add('だ', 'ダ');
            hi_ka.Add('ぢ', 'ヂ');
            hi_ka.Add('づ', 'ヅ');
            hi_ka.Add('で', 'デ');
            hi_ka.Add('ど', 'ド');
            hi_ka.Add('な', 'ナ');
            hi_ka.Add('に', 'ニ');
            hi_ka.Add('ぬ', 'ヌ');
            hi_ka.Add('ね', 'ネ');
            hi_ka.Add('の', 'ノ');
            hi_ka.Add('は', 'ハ');
            hi_ka.Add('ひ', 'ヒ');
            hi_ka.Add('ふ', 'フ');
            hi_ka.Add('へ', 'ヘ');
            hi_ka.Add('ほ', 'ホ');
            hi_ka.Add('ば', 'バ');
            hi_ka.Add('び', 'ビ');
            hi_ka.Add('ぶ', 'ブ');
            hi_ka.Add('べ', 'ベ');
            hi_ka.Add('ぼ', 'ボ');
            hi_ka.Add('ぱ', 'パ');
            hi_ka.Add('ぴ', 'ピ');
            hi_ka.Add('ぷ', 'プ');
            hi_ka.Add('ぺ', 'ペ');
            hi_ka.Add('ぽ', 'ポ');
            hi_ka.Add('ま', 'マ');
            hi_ka.Add('み', 'ミ');
            hi_ka.Add('む', 'ム');
            hi_ka.Add('め', 'メ');
            hi_ka.Add('も', 'モ');
            hi_ka.Add('や', 'ヤ');
            hi_ka.Add('ゆ', 'ユ');
            hi_ka.Add('よ', 'ヨ');
            hi_ka.Add('ら', 'ラ');
            hi_ka.Add('り', 'リ');
            hi_ka.Add('る', 'ル');
            hi_ka.Add('れ', 'レ');
            hi_ka.Add('ろ', 'ロ');
            hi_ka.Add('わ', 'ワ');
            hi_ka.Add('を', 'ヲ');
            hi_ka.Add('ん', 'ン');
            hi_ka.Add('あ', 'ア');
            hi_ka.Add('い', 'イ');
            hi_ka.Add('う', 'ウ');
            hi_ka.Add('え', 'エ');
            hi_ka.Add('お', 'オ');

            #endregion HirganaToKatakana

            #region KatakanaToHirgana
            ka_hi.Add('ヴ', 'ゔ');
            ka_hi.Add('カ', 'か');
            ka_hi.Add('キ', 'き');
            ka_hi.Add('ク', 'く');
            ka_hi.Add('ケ', 'け');
            ka_hi.Add('コ', 'こ');
            ka_hi.Add('ガ', 'が');
            ka_hi.Add('ギ', 'ぎ');
            ka_hi.Add('グ', 'ぐ');
            ka_hi.Add('ゲ', 'げ');
            ka_hi.Add('ゴ', 'ご');
            ka_hi.Add('サ', 'さ');
            ka_hi.Add('シ', 'し');
            ka_hi.Add('ス', 'す');
            ka_hi.Add('セ', 'せ');
            ka_hi.Add('ソ', 'そ');
            ka_hi.Add('ザ', 'ざ');
            ka_hi.Add('ジ', 'じ');
            ka_hi.Add('ズ', 'ず');
            ka_hi.Add('ゼ', 'ぜ');
            ka_hi.Add('ゾ', 'ぞ');
            ka_hi.Add('タ', 'た');
            ka_hi.Add('チ', 'ち');
            ka_hi.Add('ツ', 'つ');
            ka_hi.Add('テ', 'て');
            ka_hi.Add('ト', 'と');
            ka_hi.Add('ダ', 'だ');
            ka_hi.Add('ヂ', 'ぢ');
            ka_hi.Add('ヅ', 'づ');
            ka_hi.Add('デ', 'で');
            ka_hi.Add('ド', 'ど');
            ka_hi.Add('ナ', 'な');
            ka_hi.Add('ニ', 'に');
            ka_hi.Add('ヌ', 'ぬ');
            ka_hi.Add('ネ', 'ね');
            ka_hi.Add('ノ', 'の');
            ka_hi.Add('ハ', 'は');
            ka_hi.Add('ヒ', 'ひ');
            ka_hi.Add('フ', 'ふ');
            ka_hi.Add('ヘ', 'へ');
            ka_hi.Add('ホ', 'ほ');
            ka_hi.Add('バ', 'ば');
            ka_hi.Add('ビ', 'び');
            ka_hi.Add('ブ', 'ぶ');
            ka_hi.Add('ベ', 'べ');
            ka_hi.Add('ボ', 'ぼ');
            ka_hi.Add('パ', 'ぱ');
            ka_hi.Add('ピ', 'ぴ');
            ka_hi.Add('プ', 'ぷ');
            ka_hi.Add('ペ', 'ぺ');
            ka_hi.Add('ポ', 'ぽ');
            ka_hi.Add('マ', 'ま');
            ka_hi.Add('ミ', 'み');
            ka_hi.Add('ム', 'む');
            ka_hi.Add('メ', 'め');
            ka_hi.Add('モ', 'も');
            ka_hi.Add('ヤ', 'や');
            ka_hi.Add('ユ', 'ゆ');
            ka_hi.Add('ヨ', 'よ');
            ka_hi.Add('ラ', 'ら');
            ka_hi.Add('リ', 'り');
            ka_hi.Add('ル', 'る');
            ka_hi.Add('レ', 'れ');
            ka_hi.Add('ロ', 'ろ');
            ka_hi.Add('ワ', 'わ');
            ka_hi.Add('ヲ', 'を');
            ka_hi.Add('ン', 'ん');
            ka_hi.Add('ア', 'あ');
            ka_hi.Add('イ', 'い');
            ka_hi.Add('ウ', 'う');
            ka_hi.Add('エ', 'え');
            ka_hi.Add('オ', 'お');

            #endregion KatakanaToHirgana

            #region HiraganeToRomaji
            hi_ro.Add('ゔ', "vu");
            hi_ro.Add('か', "ka");
            hi_ro.Add('き', "ki");
            hi_ro.Add('く', "ku");
            hi_ro.Add('け', "ke");
            hi_ro.Add('こ', "ko");
            hi_ro.Add('が', "ga");
            hi_ro.Add('ぎ', "gi");
            hi_ro.Add('ぐ', "gu");
            hi_ro.Add('げ', "ge");
            hi_ro.Add('ご', "go");
            hi_ro.Add('さ', "sa");
            hi_ro.Add('し', "shi");
            hi_ro.Add('す', "su");
            hi_ro.Add('せ', "se");
            hi_ro.Add('そ', "so");
            hi_ro.Add('ざ', "za");
            hi_ro.Add('じ', "ji");
            hi_ro.Add('ず', "zu");
            hi_ro.Add('ぜ', "ze");
            hi_ro.Add('ぞ', "zo");
            hi_ro.Add('た', "ta");
            hi_ro.Add('ち', "chi");
            hi_ro.Add('つ', "tsu");
            hi_ro.Add('て', "te");
            hi_ro.Add('と', "to");
            hi_ro.Add('だ', "da");
            hi_ro.Add('ぢ', "ji");
            hi_ro.Add('づ', "dzu");
            hi_ro.Add('で', "de");
            hi_ro.Add('ど', "o");
            hi_ro.Add('な', "na");
            hi_ro.Add('に', "ni");
            hi_ro.Add('ぬ', "nu");
            hi_ro.Add('ね', "ne");
            hi_ro.Add('の', "no");
            hi_ro.Add('は', "ha");
            hi_ro.Add('ひ', "hi");
            hi_ro.Add('ふ', "fu");
            hi_ro.Add('へ', "he");
            hi_ro.Add('ほ', "ho");
            hi_ro.Add('ば', "ba");
            hi_ro.Add('び', "bi");
            hi_ro.Add('ぶ', "bu");
            hi_ro.Add('べ', "be");
            hi_ro.Add('ぼ', "bo");
            hi_ro.Add('ぱ', "pa");
            hi_ro.Add('ぴ', "pi");
            hi_ro.Add('ぷ', "pu");
            hi_ro.Add('ぺ', "pe");
            hi_ro.Add('ぽ', "po");
            hi_ro.Add('ま', "ma");
            hi_ro.Add('み', "mi");
            hi_ro.Add('む', "mu");
            hi_ro.Add('め', "me");
            hi_ro.Add('も', "mo");
            hi_ro.Add('や', "ya");
            hi_ro.Add('ゆ', "yu");
            hi_ro.Add('よ', "yo");
            hi_ro.Add('ら', "ra");
            hi_ro.Add('り', "ri");
            hi_ro.Add('る', "ru");
            hi_ro.Add('れ', "re");
            hi_ro.Add('ろ', "ro");
            hi_ro.Add('わ', "wa");
            hi_ro.Add('を', "wo");
            hi_ro.Add('ん', "n");
            hi_ro.Add('あ', "a");
            hi_ro.Add('い', "i");
            hi_ro.Add('う', "u");
            hi_ro.Add('え', "e");
            hi_ro.Add('お', "o");
            #endregion

            #region KatakanaToRomaji
            ka_ro.Add('ヴ', "vu");
            ka_ro.Add('カ', "ka");
            ka_ro.Add('キ', "ki");
            ka_ro.Add('ク', "ku");
            ka_ro.Add('ケ', "ke");
            ka_ro.Add('コ', "ko");
            ka_ro.Add('ガ', "ga");
            ka_ro.Add('ギ', "gi");
            ka_ro.Add('グ', "gu");
            ka_ro.Add('ゲ', "ge");
            ka_ro.Add('ゴ', "go");
            ka_ro.Add('サ', "sa");
            ka_ro.Add('シ', "shi");
            ka_ro.Add('ス', "su");
            ka_ro.Add('セ', "se");
            ka_ro.Add('ソ', "so");
            ka_ro.Add('ザ', "za");
            ka_ro.Add('ジ', "ji");
            ka_ro.Add('ズ', "zu");
            ka_ro.Add('ゼ', "ze");
            ka_ro.Add('ゾ', "zo");
            ka_ro.Add('タ', "ta");
            ka_ro.Add('チ', "chi");
            ka_ro.Add('ツ', "tsu");
            ka_ro.Add('テ', "te");
            ka_ro.Add('ト', "to");
            ka_ro.Add('ダ', "da");
            ka_ro.Add('ヂ', "ji");
            ka_ro.Add('ヅ', "dzu");
            ka_ro.Add('デ', "de");
            ka_ro.Add('ド', "o");
            ka_ro.Add('ナ', "na");
            ka_ro.Add('ニ', "ni");
            ka_ro.Add('ヌ', "nu");
            ka_ro.Add('ネ', "ne");
            ka_ro.Add('ノ', "no");
            ka_ro.Add('ハ', "ha");
            ka_ro.Add('ヒ', "hi");
            ka_ro.Add('フ', "fu");
            ka_ro.Add('ヘ', "he");
            ka_ro.Add('ホ', "ho");
            ka_ro.Add('バ', "ba");
            ka_ro.Add('ビ', "bi");
            ka_ro.Add('ブ', "bu");
            ka_ro.Add('ベ', "be");
            ka_ro.Add('ボ', "bo");
            ka_ro.Add('パ', "pa");
            ka_ro.Add('ピ', "pi");
            ka_ro.Add('プ', "pu");
            ka_ro.Add('ペ', "pe");
            ka_ro.Add('ポ', "po");
            ka_ro.Add('マ', "ma");
            ka_ro.Add('ミ', "mi");
            ka_ro.Add('ム', "mu");
            ka_ro.Add('メ', "me");
            ka_ro.Add('モ', "mo");
            ka_ro.Add('ヤ', "ya");
            ka_ro.Add('ユ', "yu");
            ka_ro.Add('ヨ', "yo");
            ka_ro.Add('ラ', "ra");
            ka_ro.Add('リ', "ri");
            ka_ro.Add('ル', "ru");
            ka_ro.Add('レ', "re");
            ka_ro.Add('ロ', "ro");
            ka_ro.Add('ワ', "wa");
            ka_ro.Add('ヲ', "wo");
            ka_ro.Add('ン', "n");
            ka_ro.Add('ア', "a");
            ka_ro.Add('イ', "i");
            ka_ro.Add('ウ', "u");
            ka_ro.Add('エ', "e");
            ka_ro.Add('オ', "o");
            #endregion

        }

        private Mode FindTranslationMode(JType detectedLanguage, string label)
        {
            Mode translationMode = Mode.Unknown;

            switch (detectedLanguage)
            {
                case JType.Hiragana:
                    if (label.Contains(JType.Katakana.ToString()))
                        translationMode = Mode.Hiragana_Katakana;
                    else if (label.Contains(JType.Romaji.ToString()))
                        translationMode = Mode.Hiragana_Romaji;
                    break;
                case JType.Katakana:
                    if (label.Contains(JType.Hiragana.ToString()))
                        translationMode = Mode.Katakana_Hiragana;
                    else if (label.Contains(JType.Romaji.ToString()))
                        translationMode = Mode.Katakana_Romaji;
                    break;
                case JType.Romaji:
                    if (label.Contains(JType.Katakana.ToString()))
                        translationMode = Mode.Romaji_Katakana;
                    else if (label.Contains(JType.Hiragana.ToString()))
                        translationMode = Mode.Romaji_Hiragana;
                    break;
            }

            return translationMode;
        }

        #endregion

        #region Properties

        public string UserInput
        {
            get
            {
                return _userInput;
            }

            set
            {
                _userInput = value;
                RaisePropertyChanged(nameof(UserInput));
            }
        }

        public string UserInputLabel
        {
            get
            {
                return _userInputLabel;
            }

            set
            {
                _userInputLabel = value;
                RaisePropertyChanged(nameof(UserInputLabel));
            }
        }

        public string OutputLabel
        {
            get
            {
                return _outputLabel;
            }

            set
            {
                _outputLabel = value;
                RaisePropertyChanged(nameof(OutputLabel));
            }
        }

        public string UserInputPlaceholder
        {
            get
            {
                return _userInputPlaceholder;
            }

            set
            {
                _userInputPlaceholder = value;
                RaisePropertyChanged(nameof(UserInputPlaceholder));
            }
        }

        public TranslationOutputs Translations
        {
            get
            {
                return _translations;
            }

            set
            {
                _translations = value;
                RaisePropertyChanged(nameof(Translations));
            }
        }

        public JType DetectedUserInputLanguage
        {
            get
            {
                return _detectedUserInputLanguage;
            }

            set
            {
                _detectedUserInputLanguage = value;
                RaisePropertyChanged(nameof(DetectedUserInputLanguage));
            }
        }

        #endregion

        #region Commands

        public RelayCommand AddTranslation { get; set; }
        public RelayCommand ResetInput { get; set; }
        public RelayCommand Translate { get; set; }

        #endregion
    }
}
