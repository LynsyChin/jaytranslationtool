﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using static JayTranslationTool.Modules.Main.ViewModels.MainWindowViewModel;

namespace JayTranslationTool.Modules.Main.Views.Converters
{
    public class JTypeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null || !(value is JType))
            {
                return string.Empty;
            }
            else
            {
                JType type = (JType)value;
                return type == JType.Unknown ? string.Empty : type.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Not required for now.
            throw new NotImplementedException();
        }
    }
}
